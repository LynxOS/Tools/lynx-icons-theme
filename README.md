# Lynx Icons Theme

A flat, colorful icon theme based on Tela Circle Icons Theme

## Installation

Usage:  `./install.sh`  **[OPTIONS...]** **[COLOR VARIANTS...]**

| OPTIONS     | Explanation                                                                   |
|:------------|:------------------------------------------------------------------------------|
| `-a`        | Install all color versions                                                    |
| `-d`        | Specify theme destination directory                                           |
| `-n`        | Specify theme name                                                            |
| `-c`        | Install circle folder versions                                                |
| `-h`        | Show this help                                                                |

| COLOR VARIANTS    | Explanation                           |
|:------------------|:--------------------------------------|
| `standard`        | Standard color folder version         |
| `black`           | Black color folder version            |
| `blue`            | Blue color folder version             |
| `brown`           | Brown color folder version            |
| `green`           | Green color folder version            |
| `grey`            | Grey color folder version             |
| `orange`          | Orange color folder version           |
| `pink`            | Pink color folder version             |
| `purple`          | Purple color folder version           |
| `red`             | Red color folder version              |
| `yellow`          | yellow color folder version           |
| `manjaro`         | Manjaro default color folder version  |
| `ubuntu`          | Ubuntu default color folder version   |

By default, only **the `standard` variant** is selected.

